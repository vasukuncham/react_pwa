import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
});

export default function HomePage() {
  const classes = useStyles();
  const [spacing] = React.useState(2);

  return (
    <Container>
      <Grid item xs={12}>
        <Grid container spacing={spacing}>
          {[0, 1, 2, 3, 4, 5].map(value => (
            <Grid key={value} item xs={12} sm={4} md={3} lg={3}>
              <Card className={classes.card}>
                <CardActionArea>
                  <CardMedia
                    component="img"
                    alt="Product Image"
                    height="160"
                    image="https://material-ui.com/static/images/cards/contemplative-reptile.jpg"
                    title="Product Image"
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                      Product title
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                      sample description about the product. sample description about the product.
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions>
                  <Link to="/cart">
                    <Button size="small" color="primary">
                      Add to Cart
                    </Button>
                  </Link>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Container>
  );
}