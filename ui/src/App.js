import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import AppHeader from './components/AppHeader';
import HomePage from './pages/HomePage';
import CartPage from './pages/CartPage';

function App() {
  return (
    <div className="App">
      <AppHeader />

      <Router>
          <div>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/cart" component={CartPage} />
          </div>
      </Router>
    </div>
  );
}

export default App;
